package com.oschina.sequence.test;

import com.oschina.sequence.DatabaseConfig;
import com.oschina.sequence.SequenceManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by limu on 15/10/15.
 */
public class SequenceGroupTest {


    public static void main(String[] args) throws Exception {
        AtomicLong success = new AtomicLong(0);
        AtomicLong fail = new AtomicLong(0);

        int MAX_THREAD_NUM = 5;

        SequenceManager sequenceManager = new SequenceManager();

        List<DatabaseConfig> databaseConfigs = new ArrayList<>();

        DatabaseConfig databaseConfig = new DatabaseConfig();
        databaseConfig.setHost("127.0.0.1:3306");
        databaseConfig.setUserName("root");
        databaseConfig.setPassword("123456");
        databaseConfig.setDatabaseName("trade");
        databaseConfigs.add(databaseConfig);

        DatabaseConfig databaseConfig1 = new DatabaseConfig();
        databaseConfig1.setHost("127.0.0.1:3306");
        databaseConfig1.setUserName("root");
        databaseConfig1.setPassword("123456");
        databaseConfig1.setDatabaseName("trade1");
        databaseConfigs.add(databaseConfig1);

        sequenceManager.setDatabaseConfigs(databaseConfigs);
        sequenceManager.setIdName("bizOrderId");
        sequenceManager.setAppName("trade");
//        sequenceManager.setTableSize(4);

        sequenceManager.init();

        long start = new Date().getTime();
        ExecutorService service = Executors.newFixedThreadPool(MAX_THREAD_NUM);
        for (int i = 0; i < MAX_THREAD_NUM; i++) {
            service.execute(new SequenceGroupTestThread(sequenceManager, success, fail));
        }

        service.shutdown();
        while (!service.isTerminated()) {
            // 等待所有子线程结束，才退出主线程
        }

        sequenceManager.destroy();

        long end = new Date().getTime();

        System.out.println("\r\n\r\n\r\n\r\n**************************\r\n\r\n\r\n\r\n\r\n");
        System.out.println("tps=" + (success.get() + fail.get()) * 1000 / (end - start));
        System.out.println("success=" + success.get());
        System.out.println("fail=" + fail.get());
        System.out.println("success pec=" + success.get() * 1.0 / (success.get() + fail.get()));
        System.out.println("\r\n\r\n\r\n\r\n**************************\r\n\r\n\r\n\r\n\r\n");

    }


}
